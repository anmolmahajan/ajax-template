var app = require('express')();
var parser = require('body-parser');

app.use(parser.urlencoded({extended: true}));
app.get('/', function(request, response){
	response.sendFile(__dirname + '/public/index.html')
});

app.get('/books', function(request, response){
	response.send({"firstName": "Sung", "lastName": "Harsh"});
});


var abc = ['Titanic Movie', 'Titanic Soundtrack', 'Inception Movie', 'Inception Soundtrack']

app.get('/formSubmit', function(request, response){
	var str = request.body.username;
	var ret_str = "";
	abc.forEach(function(value, index, arr){
		if(value.search(str) != -1){
			ret_str += value + ",";
		}
	});
	response.send(ret_str);
});

app.listen(6969, function(){
	console.log('Port: 6969');
});